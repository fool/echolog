<?php

/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Jordan Sterling
 * 4/21/14
 */

namespace fool\echolog;

use \Psr\Log\LoggerInterface;
use \Psr\Log\LogLevel;

/**
 * Logs using echo
 */
class Echolog implements LoggerInterface
{
    /**
     * @var callable
     * @see Echolog::defaultMessageFormatter()
     */
    private $messageFormatter;

    /**
     * @var string
     */
    private $level;

    /**
     * @var int[]
     */
    protected static $rankings = array(
        LogLevel::DEBUG     => 7,
        LogLevel::INFO      => 6,
        LogLevel::NOTICE    => 5,
        LogLevel::WARNING   => 4,
        LogLevel::ERROR     => 3,
        LogLevel::CRITICAL  => 2,
        LogLevel::ALERT     => 1,
        LogLevel::EMERGENCY => 0,
    );

    /**
     * @param string $level
     */
    public function __construct($level = LogLevel::DEBUG)
    {
        $this->level = $level;
        $this->messageFormatter = array($this, 'defaultMessageFormatter');
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function emergency($message, array $context = array())
    {
        $this->log(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function alert($message, array $context = array())
    {
        $this->log(LogLevel::ALERT, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function critical($message, array $context = array())
    {
        $this->log(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function error($message, array $context = array())
    {
        $this->log(LogLevel::ERROR, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function warning($message, array $context = array())
    {
        $this->log(LogLevel::WARNING, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function notice($message, array $context = array())
    {
        $this->log(LogLevel::NOTICE, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function info($message, array $context = array())
    {
        $this->log(LogLevel::INFO, $message, $context);
    }

    /**
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function debug($message, array $context = array())
    {
        $this->log(LogLevel::DEBUG, $message, $context);
    }

    /**
     * @param  string $level
     * @param  string $message
     * @param  array  $context
     * @return void
     */
    public function log($level, $message, array $context = array())
    {
        $hasLevel = isset(self::$rankings[$level]);

        if (!$hasLevel || ($hasLevel && self::$rankings[$level] <= self::$rankings[$this->level])) {
            $formatter = $this->messageFormatter;
            echo $formatter($level, $message, $context);
            flush();
        }
    }

    /**
     * @param  string $level
     * @param  string $message
     * @param  array  $context
     * @return string
     */
    protected function defaultMessageFormatter($level, $message, array $context = array())
    {
        $message = sprintf('[%s] %s %s', date('Y-m-d H:i:s'), strtoupper($level), $message);
        if ($context) {
            $message .= ' ' . json_encode($context);
        }
        return $message . PHP_EOL;
    }

    /**
     * @param  callable $messageFormatter
     * @return void
     */
    public function setMessageFormatter(callable $messageFormatter)
    {
        $this->messageFormatter = $messageFormatter;
    }

    /**
     * @param  string $level
     * @return void
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }
}
